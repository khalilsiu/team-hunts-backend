import * as mongoose from 'mongoose'

const GameSchema = new mongoose.Schema({
    name: String,
    duration: Number,
    startLocation: {
        name: String,
        long: Number,
        lat: Number
    },
    endLocation: {
        name: String,
        long: Number,
        lat: Number
    },
    tasks: [{
        taskId: Number
    }],
    started: Boolean,
    startTime: Date,
    teams: [{
        name: String,
        code: String,
        members: [{
            name: String,
            isLeader: Boolean
        }],
        currentLocation: {
            long: Number,
            lat: Number,
            timestamp: Date
        },
        tasks: [{
            taskId: Number,
            answer: String,
            accepted: Boolean,
            timestamp: Date
        }]
    }],
    chatId: Number
})

export default mongoose.model('Game', GameSchema)