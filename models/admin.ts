import * as mongoose from 'mongoose'


let adminSchema = new mongoose.Schema({
    name: String,
    email: String,
    hash: String,
    phone: String,
    gameId: [String]
})

export default mongoose.model('Admin', adminSchema)

