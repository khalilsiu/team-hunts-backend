import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as session from 'express-session'
import * as passport from 'passport'
import * as mongoose from 'mongoose'
import * as graphqlHTTP from 'express-graphql'
import schema from './schema/schema'
import {Request, ErrorRequestHandler, Response, NextFunction} from 'express'



const app = express();

app.use(session({
    secret: 'Team-hunts',
    resave:true,
    saveUninitialized:true
}));

mongoose.connect('mongodb+srv://anthony:dbs123@cluster0-csy6z.mongodb.net/test?retryWrites=true&w=majority',{
    useUnifiedTopology: true
})

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log('db connected')
});


app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(cors());

app.use(passport.initialize())
app.use(passport.session())
// you need to include your own passport for the strategy to be used
import './passport'
import {isLoggedIn} from './guard'
import { login } from './schema/Auth'


app.use('/graphql',isLoggedIn, graphqlHTTP({
  schema,
  graphiql: true
}))

app.post('/login', async (req, res, next)=> {
  try{
    const token = await login(req.body.email, req.body.password)
    console.log(token)
    res.json({isSuccess: true, token});
  }catch(e){
    res.status(401).json({ isSuccess: false, error: e.toString() })
  }
})

app.use(errorHandler)

function errorHandler (e: ErrorRequestHandler, req:Request, res: Response, next: NextFunction) {
  res.status(500)
  res.json({ isSuccess: false, error: e.toString() })
}

const PORT = '8080';


app.listen(PORT, ()=> console.log('listening on port '+ PORT))